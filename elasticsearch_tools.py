# -*- coding: utf-8 -*-
"""
Created on Sun Apr 15 01:42:50 2018

@author: Nicholas Vercruysse
"""

from elasticsearch import Elasticsearch
import json

class ElasticSearchTools:
    
    def __init__(self):
        self.client = Elasticsearch(["https://search-dynamo-hackathon-luhe6v5vah7nd5nrswov76xwue.us-east-1.es.amazonaws.com"])

    def search(self, index, doc_type, body):
        return self.client.search(index=index, doc_type=doc_type, body=body)
    
    def delete_on_id(self, index, doc_type, _id):
        return self.client.delete(index=index, doc_type=doc_type, id = _id)
    
    def insert(self, index, doc_type, _id, body):
        return self.client.create(index=index, doc_type=doc_type, id = _id, body=body)
    