# -*- coding: utf-8 -*-
"""
Created on Sat Apr 14 20:53:58 2018

@author: Nicholas Vercruysse
"""


import json
import pandas as pd
from elasticsearch_tools import ElasticSearchTools
import os
import time
from datetime import datetime
import random


#This won't parse JSONs... elasticsearch does that for you and it's awesome
#So this is now general get stuff done

parent_folder = os.path.dirname("C:\\Users\\Nicholas Vercruysse\\Documents\\Hackathon\\people\\people\\")
contents = list(os.walk(parent_folder))[1:]
json_dict = {}
json_dict['uploader'] = '123.12.123.12'
json_dict['unix_date'] = 123 #will be changed based on json in file
json_dict['ipfs'] = 123 #placeholder, likely won't change, not using it for mock data
json_dict['category'] = 0 #same as above
json_dict['mimetype'] = 'application/json' #true for all
json_dict['data'] = {} #payload
do_not_push = ['0cac66652c768fcc7f09c6d609653e1c',
               '166f4b2a469c2b733034cc6a4e118c6b',
               '16ab6b133d4b9b9f00fe48deb54862fb',
               '1fa72f172e0ead323004ead4035efdf3',
               '270f535f0b5105276ce8861faed1ff7f',
               '278d46cd6b634b9727bd26851966ebff',
               '304b5c74d4da6b9525ef1962f5f64bd5',
               '496cdc9ac4c044349e63dd3fe7bac063',
               '4a471b1b2a5edf3612cc2317eb02ddf6']

weekends = []
weekends.append(int(time.mktime(
                  datetime.strptime(
                    '2018-03-08 14:24:00', '%Y-%m-%d %H:%M:%S').timetuple())))
weekends.append(int(time.mktime(
                  datetime.strptime(
                    '2018-03-15 14:24:00', '%Y-%m-%d %H:%M:%S').timetuple())))
weekends.append(int(time.mktime(
                  datetime.strptime(
                    '2018-03-22 14:24:00', '%Y-%m-%d %H:%M:%S').timetuple())))


weekcounters = [0, 0, 0]
weeklimits = [7, 4, 2]
jsons = {}
for x in contents:
    hash_hex = x[0].split("\\")[-1]
    if (hash_hex in do_not_push):
        continue
    json_loc = x[0] + '\\person.json' # will only read!
    data = {}
    with open(json_loc) as data_json:
        json_lines = data_json.readlines()
        json_str = "".join(json_lines)
        try:
            data = json.loads(json_str)
        except:
            print(json_lines)
        if (weekcounters[0] < weeklimits[0]):
            data['unix_date'] = weekends[0] + random.randint(20, 1000)
            weekcounters[0] += 1
            json_dict['data'] = data
            json_dict['unix_date'] = data['unix_date']
            json_str = json.dumps(json_dict)
            jsons[data['hash']] = json_str
        elif (weekcounters[1] < weeklimits[1]):
            data['unix_date'] = weekends[1] + random.randint(20, 1000)
            weekcounters[1] += 1
            json_dict['data'] = data
            json_dict['unix_date'] = data['unix_date']
            json_str = json.dumps(json_dict)
            jsons[data['hash']] = json_str
        elif (weekcounters[2] < weeklimits[2]):
            data['unix_date'] = weekends[2] + random.randint(20, 1000)
            weekcounters[2] += 1
            json_dict['data'] = data
            json_dict['unix_date'] = data['unix_date']
            json_str = json.dumps(json_dict)
            jsons[data['hash']] = json_str
print(jsons)
print(len(jsons))
print(weekcounters)
print(weekends)
client = ElasticSearchTools()
counter = 0
succesful_hex = []
for k, v in jsons.items():
    result = client.insert('missing_person', 'json', k, v)
    print(result)
    succesful_hex.append(k)
print(succesful_hex)





