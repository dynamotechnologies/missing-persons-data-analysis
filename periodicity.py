# -*- coding: utf-8 -*-
"""
Created on Sat Apr 14 15:04:35 2018

@author: Nicholas Vercruysse
"""

import pandas as pd
from statsmodels.tsa.arima_model import ARIMA as arima
from pandas.plotting import autocorrelation_plot
import matplotlib.pyplot as plt
import numpy as np
from statsmodels.tsa.stattools import adfuller
from statsmodels.tsa.statespace.sarimax import SARIMAX as sarimax

def test_stationarity(timeseries):
    rolmean = pd.rolling_mean(timeseries, window=12)
    rolstd = pd.rolling_std(timeseries, window=12)
    
    plt.plot(timeseries, color='blue')
    plt.plot(rolmean, color='red')
    plt.plot(rolstd, color='black')
    plt.show()
    
    dftest = adfuller(timeseries, autolag='AIC')
    dfoutput = pd.Series(dftest[0:4], index=['Test Statistic', 'p-value', '#Lags Used', "Number of Observations Used"])
    for k, v in dftest[4].items():
        dfoutput['Critical Values (%s)'% k] = v
    print(dfoutput)

def genSine(x):
    sinusoid = np.sin(x) + np.random.normal(scale=0.68, size=len(x)) + x / 6 + 2
    return sinusoid

def genMaleSine(x):
    sinusoid = np.sin(x) + np.random.normal(scale=0.68, size=len(x)) + 10 - x / 8
    return sinusoid



periods = pd.read_csv('synthetic', delim_whitespace=True,
                      header=None)
##SARIMAX (p,d,q)x(P,D,Q,s) 
##d + D*s + max(3*q + 1, 3*Q*s + 1, p, P*s) + 1 observations
x = np.linspace(1, 40, num=400)
period_series = pd.Series(genSine(x))
period_series_m = pd.Series(genMaleSine(x))
period_series.index = period_series.index * 60 * 60 * 24 * 7 + 24 * 60 * 60 * 365 * 40.64
period_series.index = pd.to_datetime(period_series.index, unit='s')
period_series_m.index = period_series_m.index * 60 * 60 * 24 * 7 + 24 * 60 * 60 * 365 * 40.64
period_series_m.index = pd.to_datetime(period_series_m.index, unit='s')
period_series.to_csv('series_f.csv')
period_series.to_csv('series_m.csv')

model = sarimax(period_series, trend='n', order=(0, 1, 1), seasonal_order=(1, 1, 1, 64))
results = model.fit()
print(results.summary())
forecasts = results.predict(start = 336, end= 349, dynamic = True)
forecast_series = pd.Series(forecasts)
plt.plot(period_series)
plt.plot(forecasts)


model_m = sarimax(period_series_m, trend='n', order=(0,1,1), seasonal_order=(1, 1, 1, 64))
results_m = model_m.fit()
forecasts_m = results_m.predict(start = 336, end = 349, dynamic = True)
forecast_series_m = pd.Series(forecasts_m)
plt.plot(period_series_m)
plt.plot(forecast_series_m)
plt.show()
forecasts.to_csv(path='predictions_f.csv')
forecasts_m.to_csv(path='predications_m.csv')

